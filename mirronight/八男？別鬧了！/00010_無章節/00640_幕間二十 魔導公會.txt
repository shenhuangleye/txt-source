﻿幕間二十 魔導公會

「從魔導來的招待信嗎？」

那個地獄般的地下迷宮總算攻略完成了，終於連獎賞的事情也解決的時候。

我不知道為什麼從布蘭塔庫先生那裡，收到了從魔導工會總部送過來的招待信。

「送到在王都布萊希萊德邊境伯宅邸，是寄來給我的」

「布蘭塔庫先生是魔導工會的會員啊」

「並不是特別(對魔導工會)有好感」

布蘭塔庫先生把要交給我的信當作扇子一邊搧來搧去一邊回答我的問題。

所謂的魔導工會，如同字面上那樣是能使用魔法的魔法使所屬的工會。

會員總數約二千人。

從魔法使的總數來思考的話人數是很稀少的，但也不是一個鄉下農夫可以施放火種一般程度(魔法)的人就能成為會員。

另一方面，製作魔道具的人是隸屬魔道具工會，那個部份的人數更是少之又少。

「咦？ 不能同時所屬兩個工會嗎？」

其他工會的話，例如我在孩子的時候就得到了商業工會所發行的會員證，現在還持有冒險者工會的會員證。

其他也有很多人兼任複數工會，工會方面會員數越多就有種種的好處，不用說這是很平常的。

只有，魔導工會與魔道具工會是無法兼任的。

不可思議的故事。

「重要的並不只是辦不到而已……」

從以前彼此的關係就不好了，但現在還有個理由讓這兩者的關係更緊張。

「預算的分配上……」

「牽扯到錢問題就很嚴重呢」

「就是啊」

赫爾姆特王國成立後經過一段時間，王國的收成開始富裕時，為了使魔法文明時代的優秀魔法技術得以恢復，編列預算用於魔法技術的研究。

然而，魔法使的數量不足。

許多公務部門，都不能輕易地募集到人。

於是，把預算交給魔道具工會與魔導工會，委託他們進行魔導技術研究。

從這一刻開始，兩個工會就被大眾所承認是半個公務機關。

「不管哪邊取得成果，都會在預算分配上起爭執。還有，看情況」

是微妙地可悲的話題，不過這樣的故事並不是太稀奇。

不管到世界的哪個地方也都會有終於人類本質的傢伙吧。

「魔導工會為了與魔道具工會對抗，策劃增加會員的人數」

另一方面魔道具工會，這裡不會製作魔道具是不能組織起來。

並且，至少沒有沒有會員無法製作出通用類型的魔道具。

因此會員數很少，魔道具就像從世上牽引的風箏一般也象徵工會的存在感不會動搖。(潤色後:因有製成的物品被需求、使用所以很醒目，工會的存在感也因此不受動搖)

魔導工會的做法是強硬地將著名的魔法使成為會員，作為與魔道具工會的競爭手段。

「打從心底，怎樣都好啦」

「俺的內心也這麼想」

魔法，基本上是個人單獨學習得來的。

有師父的人也很多，另外既使沒有加入魔導工會也能生活。

沉默以對的話，誰也都不會來做會員登記。

這樣的話連我在記憶的角落也不會當作有魔導工會這組織的存在。

「師傅已經是會員了嗎？」

「聽說隨隨便便就被登錄了。我也是一樣」

「這樣不錯的組織，被當作半個公務機關呢」

「共同魔法陣的研究委託。但，在那還是有少數優秀的魔法使喔」

共同魔法陣，依賴魔法使個人的思考・想像能力的魔法，只要任何人有魔力就能夠使用的東西。

幾日前，強制轉移魔法陣就是相同的東西這樣子說就很容易懂了。

那個魔法陣是用其他魔法製作出來的，最終盡其將各種各樣的魔法的魔力聚攏後做出再發動為目的的集合魔法陣。

「原來如此，魔法陣的條件就像探詢翻動書本的內頁，即是使用魔法時魔力被聚攏後發動」 (原句:なるほど、魔法陣の本を卷って使いたい魔法をのページを探し、それに魔力を篭めて発動と。有誤請幫忙指正)

「是那種感覺啊。少部分的魔力利用魔晶石填補就可以了」

快速反應上較差，卻可以複數同時使用相同的魔法。

對軍隊來說，這說不定是相當方便。

「不過，古代魔法文明時期的魔法陣啊……」

轉移或強制轉移多半屬其他類型的攻擊魔法，陷阱發動後(結構本體)有相當的部分會消失並且無法再觸發效果也沒有辦法回收。

「魔法陣書寫的文字與記號，近似於意義不明的花紋與圖案。圖案太過於複雜，(目前)很難能有什麼成果出現」

另一方面魔道具工會那邊，逐漸有成果展現出來。

社會上有相應種類的魔道具普及著，所以不管是誰都能明白的成果。

原來如此，魔導工會開始焦急了。

「在那邊。前幾天地下迷宮攻略的時候獲得了新的魔法陣，(兩個工會)共同向將它出售的小子，對你表達感謝之意」

「對會員做(感謝)？」

「正確答案」

因為這樣的理由，我和布蘭塔庫先生拜訪位於王都中心區域的魔導工會總部。

總部的建築物，從事前的說明可以想見這東西比想像中還要氣派。

然後，它的對面有同樣相似的豪奢建築物也建在那。

「這就是魔道具工會的總部啊」

「既然互相討厭的話，為什麼還要蓋在對面……」

「之前搬家，想給予社會大眾贏家是我的印象」(世間様，東京方言。關西腔就想打人了，很怕句子跑北海道或是廣島方言出來)

「哈啊……」

被這種太白痴的理由給嚇傻了，我們在一樓的接待處告知來訪的目的後向會長所在的房間移動。

不愧是魔導工會，要說的話最高統帥與那誇張的頭銜並不相襯。

「初次見面，我是貝倫德・卡爾哈因資・瓦拉許(ベルント・カールハインツ・ヴァラハ Bernd・Karl-Heinz・Wallach)」

魔導工會的會長，無論到哪裡看起來都是個普通白髮老人。

穿著魔法使長袍的身姿，但卻看起來不像個了不起的魔法使。

仔細看，魔力好像只有初級到中級之間左右吧？

總之，這邊也趕緊自己介紹一下。

「今天，很感謝您的光臨。請趕緊……」

我第一次到這個地方來，向魔導工會做會員登錄。

會長按了呼叫鈴後，很快一個年約二十歲左右的女性職員進到裡面來並交給我會員證。

「那個什麼東西也沒填寫是？」

「是的，鮑威斯麥男爵大人的身份沒問題」

「是這樣子嗎」

只是從那個年輕的女性交付會員證後，手續就算是完成了。

大概是想真的希望入會，對方連要寫入的必要事項都是隨便應付了事。

更透過交過來的會員證仔細看，看見上頭那裡記載了名譽董事。

突然的，就被賦予名譽職的董事職位了。

「那個，名譽董事？」

「是的，鮑威斯麥男爵大人是一位優秀的魔法使」

總之，就是將名字借給魔導工會來做為宣傳這樣的事。

但是，那個名譽董事若是被剝奪時間去做什麼工作是很討厭的，那方面(如果來請託)打算拒絕。

但對手很快就會預想到這邊的意圖來做某些反駁。

「名譽會員就真的只是掛名而已。請看看您身邊同行的布蘭塔庫先生」

「俺也是名譽會員。什麼工作也都沒派給我」

反之，連報酬也都沒有。

並且，像是其他工會的會員費用也都不用支付。

因為不太會有事情的工會，有會員退會時會增加徵收年會費。

但是，話聽越覺得不明白這個組織到底存在是來做什麼這樣的感覺。。

「研究部門現在正拚命的進行魔法陣的研究」

我們找到了那個強制轉移魔法陣，要前往那個新樣式的解析那裡去。

為了研究的關係從王國發出了補助金，從極少數奇特的人得來的捐款用作魔導工會的營運上。

「請趕緊帶路吧」

向他們說其他的東西並不想看。

由先前那位年輕的女性職員的導引開始向研究部門所在的地下層移動。

「布蘭塔庫先生，那個會長……」

說得不好聽一點，怎麼看不像是個大魔法使。

所以，試著向布蘭塔庫先生詢問那個(能夠擔任會長的)理由。

「那個啊，就優秀的魔法使來說出現在此，接下來要去研究部門」

(原句:そりゃあ、優秀な魔法使いなら現場に出るか、これから行く研究部門行きだからな。這句布蘭塔庫只回答一半就把話題轉走。另可譯為:因為你太優秀了難免會被人拿來與會長比較，現在不談這些趕緊前往研究部門吧)

結論就是作為魔法使立場很微妙，而有事務能力的人就作為組織營運來使用。

所以，會長說起來未必一定是優秀的魔法使不可。

「還有以貴族的子弟就業優先」

稅金投入營運的組織，縱使勉強有魔力不過想在現場能有活躍的表現是很嚴峻的。

據說那樣的人，也有進入營運組織的方法。

「又因接受教育後，從事事務性質的工作就沒什麼大問題了。之後……」

布蘭塔庫先生用手指著在前方帶領我們往前走的年輕女性職員的下巴。

在王都內居住，勉強算得上魔法使的人對待女性，在結婚以前就只是讓她們坐在凳子上面工作嗎？有些人在結婚後也還有職員身份這種情況很多。

「事務與管理部門這類，基本都是政府機關的工作。跟魔法比較沒有關係」

「魔導工會也就是說……」

優秀的人才，前往現場或研究部門。

不是上述的人，前往工會組織或是營運部門。

確實，這樣子做很合理。

「請往這邊」

在大姊姊的帶領下進入了地下的研究室，在那裡確實有魔法使的男男女女，新的魔法陣試作與解析等忙得不可開交。

魔力方面，看到的感覺是中等級同時也是複數的存在。

「這裡是魔導工會的心臟部位」

這麼說好像很壞，要是現在這上面樓層被吹飛了會長以下的職員們都會全滅，也會完全使得魔導工會的營運出現妨礙。

正是他們研究部門才是這個魔導工會的肝。

布蘭塔庫先生小聲地向我說明。

「喔哦！ 是賣了新魔法陣的鮑威斯麥男爵殿下嗎！」 (おおっ！看到這句不禁讓人想接:迷途の子羊な。看過番外2會很有感)

注意到我們到來，一個剛步入老年的男性聲音傳了過來。

頭髮斑白亂糟糟的頭髮隨意的披在背上，那個人就像研究人員該有的感覺，他是研究部門的首位名叫盧卡斯・古茲・碧根鮑華(ルーカス・ゲッツ・ベッケンバウアー Lucas・Götz・Beckenbauer)。

「布蘭塔庫。阿爾弗雷德的弟子，持有很棒的魔力呢」

「是吧」

看起來，這兩個人是認識的樣子。

互相不拘泥的交談著。

「好，有這等魔力的話。鮑威斯麥男爵殿下，來這裡」

碧根鮑華氏，不打算做形式般的嚮導出入於研究部門內外。

拉著我的手，強行拉著前往自己的研究空間。

「布蘭塔庫先生？」

「他就是這樣的男人唷。所謂的研究笨蛋吧？」

據我的判斷，碧根鮑華氏的魔力就算在中級裡也是位居上位。

做普通的冒險者從事那方面的工作能賺更多錢，卻把時間花費在魔導工會的研究上。

這裡的人們，大多有那種感覺。

「這個是前幾天，從鮑威斯麥男爵殿下那裡購買來的魔法陣改良之後的東西」

「布蘭塔庫先生你知道嗎？」

「不，這看起來有什麼誤會……」

那個魔法陣的花紋如果仔細瞧會有頭暈目眩感，我與布蘭塔庫先生，我們自身從事魔法陣的研究自認是不可能的。

「這個能移動到哪呢？」

「不，偶然的成果，但是這個是展現出相反的效果的魔法陣的試驗品」

「相反效果嗎？」

「嗯，反過來從別的地方移動來這裡的魔法陣喔」

碧根鮑華氏的說明，這個魔法陣好像能把人與物體拉到這個魔法陣上面的效果。

「效果是可以理解，那能將哪邊的東西拉過來呢？」

「那部份就是這個魔法陣試驗品要做的理由」

與普通的魔法一樣，使用魔法陣的魔法使是需要想像力的。

「亂七八糟的說明也沒有意義。試著做看看吧。這就像……」

碧根鮑華氏站在魔法陣前，閉上眼睛約十秒一邊集中精神。

於是，一瞬間魔法陣發出青白色的光芒，接下來的瞬間不知道是什麼白色的物體放置在那上面。(招喚完成!您獲得了【白色】胖次一條。)

「這是什麼？」

「胖次……」

在魔法陣上的，放著的是白色女性用的胖次。(σ≧▽≦)σ

「那個碧根鮑華先生？」(不是我打錯，生肉裡面敘述部分是稱碧根鮑華氏，只有這句是叫他先生)

「那個是女性職員穿的吧，胖次被移動過來了」

從碧根鮑華氏衝擊性的發言，研究室內的人們視線全部都轉過來看向帶領我們來這裡的女性職員。

突然間，由於毫無理由被關注的她，一邊在她的臉變得通紅一邊由於憤怒的關係身體不體的震動。

「就像這樣，這個魔法陣能吸引過來的目標物的大小、重量、距離會因輸入的魔力量產生變化。理論上是可能可以跨越時間與次元，但是魔力的消費量就差別很大……」

「突然做了什麼！」

那個女性職員對著正在用認真的表情說明的碧根鮑華氏臉頰上賞了耳光，然後將魔法陣上的胖次搶過來拿走了。

之後，只剩臉頰有著巴掌印的碧根鮑華氏留在原地。

「咱是這個研究部門的首席……」 (ワシ與われわ，一個是男性一個女性在動漫圈裡面老人家或是擺老的人常使用。可以翻譯成我或是咱；女性就是老身。想研究請搜尋:日文語的第一人稱代名詞)

「真是的，那是你的錯吧」

布蘭塔庫先生說的對，不只我們連其他職員們也同時點頭。

「那個是有趣的東西嗎？」

「偶然之下的產物這一點上，在研究者看來是失敗的作品」

強制轉移魔法陣改良中偶然誕生的，從別的地方的東西傳送到魔法陣上。

那個威力，我和布蘭塔庫先生都清清楚楚的看見了。

實驗時那條穿過的胖次被搶走了，從那個女性職員送來那個復仇就是往碧根鮑華氏賞了把耳光，我把視線往那個魔法陣送去。

只是，無論怎麼看眼前的魔法陣跟之前的似乎看不出來差異多少。

大概，我想這是我永遠不能理解的。

「實際上，要使用看看嗎？」

「可以嗎？」

「老實說，那個成功率不是很高呢。危險性也少」

被傳送來的東西那個位置是無法顯示正確的形象，所以魔力也就因此白白的被浪費了。

碧根鮑華氏，是將視線中那個女性職員所穿的胖次當作目標，大致上是有理由的。

「如果把男性穿的內褲傳送過來，那樣可是開心不起的啊」

「這種說法還能接受……」

『為什麼要拘泥於胖次呢？』會有這樣疑問的，就剩我了吧。 

「這麼說來，理論上是可能穿越時間與次元？」

「理論上是的喲」

所謂的次元，這個世界也存在其他相似的世界是一種平行世界的概念。

怎樣都行，古代魔法文明時代有留下把異世界的產物透過魔法傳送過來的傳說。

那個到底是事實還是故事就不知道了。

「（在這裡，那個來自益世界的人……）」

正確的情況是轉生啦還是憑依啦都不知道，確實我可以肯定有異世界的存在。

但是沒有能被相信的方法。

「那麼，馬上再來」

就這樣，我也嘗試使用魔法陣。

只是這個魔法陣魔力消耗量，跟物體的重量、距離畫上等號。

遠距離把沉重的物品傳送過來是必需要龐大的魔力。

而且，腦中組成的形象失敗只是白白浪費魔力而已。

「這樣的事，時間與次元不同嗎？」

「消耗的魔力是天差地別。咱的話，在達成目的之前魔力就耗光昏死過去了」

要是持有中級上位魔力的碧根鮑華氏都那樣的話，從異世界將物體傳送過來就必須要相當大量的魔力。

第一，如果沒有那個對象物體的形象魔力只是白白浪費。

首先，安全的對策是構築在附近的東西的影像。

「什麼東西比較好呢？」

「鮑威斯麥男爵殿下，不將影像固定下來的話……」

具體到底是什麼沒有仔細考慮過，站在魔法陣前集中了也不太好吧？

魔法陣發出青白色光芒後，那上面似乎跟剛才一樣的東西被放著。

仔細一看，果然是女性用的胖次。

這個世界的貼身衣物與地球相同都是普及的東西。

像我的老家一樣的鄉下地方是自製的南瓜胖次，王都與城市是有專門的服飾店以洗練的設計製作貼身衣物。(カボチャパンツ，另外也翻譯叫作燈籠褲基本款參考下圖)

這個，也不至於是王侯貴族御用的款式，那麼有價值的貼身衣物。

「顏色是黃色的，女性用」

「背面還有兔子圖案的刺繡……」

這個胖次的主人，似乎相當喜歡可愛的東西。

「誰的東西來著？ 小子」

「撒啊(不知道)？ 其實，影像是宅邸內的東西而已」

我的房子裡頭的東西是什麼樣的影像，這是誰衣櫃裡東西被傳送過來的可能性很高。

「姑且不談影像成功與否。真不愧是鮑威斯麥男爵殿下。那麼，這條胖次……」

碧根鮑華氏用手拿起那條胖次，確認那個物體是溫的。

本人是研究者用目視確認傳送罐來的東西，但是從旁觀者的我只看見對胖次很執著的變態老頭而已。

「嗯，可能跟剛才咱的影像重疊了吧。這條胖次，無疑是誰身上穿著的東西沒錯」

「哎，是這樣嗎？」

如果是這樣的話，就發生了意想不到的事情了。

當這麼想的時候，突然研究室的門很有氣勢的被打開了。

室內，一個女性闖了進來。

那個女性，今天在屋子裡休息應該就只有伊娜一個人。

「威爾！我(私)的胖次！」

「我知道了」

「突然穿著的胖次消失了，就只有魔法辦的到」

並且，知道預定今天我要來魔導工會詢問。

為了奪回胖次急急忙忙趕往魔導工會，親切的女性職員便引領到這個地下研究室。

沒錯，就是剛才從碧根鮑華氏搶走胖次的女性職員。

「歐喔喔！突然第一次就將住在上級貴族屋子的女性穿的胖次招喚了過來。真是出色的才能啊。但是，眼前這位是冰山美人系的美女，胖次卻是可愛系的。這個就是所謂的反差萌……」

「比起那件事情！ 胖次快還給我！」

還緊緊握著胖次的關係，碧根鮑華氏被強烈的耳光炸裂了。

或者說，這個人難道不知道廢話這麼多嗎？

確實，『雞不叫的時候會啄人的』就是在講這傢伙。 (註:雉も鳴かねば撃たれまい是石川縣的民間故事也是典故。不必要的話說多了，會替自己招來災難)

「那個，伊娜」

「什麼事？ 威爾」

「下次，一起去買內衣吧」(這句用機翻會變成跟內衣交往)

「……嘛，好吧」

「小子被迷上了真是太好了吶」

免於從伊娜那邊來的制裁，我放心下來了。

「威爾這次可要召喚胖次以外的東西喔」

「這個控制方式還不太習慣耶。還只是第二次而已」

「還不習慣，也不能只召喚胖次這種難為情的東西吧」

「又再次被這麼說，還真是難為情……」

終於將胖次拿回來的伊娜也加入了，我的招喚時間再次開始。

或者說，不知不覺間就開始實驗了起來。

稱作召喚那個成果說起來很微妙。

充其量只能說把東西拿過來這樣的水準而已。

「總之，只能是胖次以外的東西」

「我知道啦……」

在伊娜以強硬的態度說話的時候，我再次從自己的屋子不知道有拿到什麼東西的影像浮在我的腦海裡。

於是第三次魔法陣發出青白色的光芒，這次是黑色不知何物的上半部被留在那。

「耶多……」

那個黑色的物體是被俗稱叫做布拉甲(胸罩)的東西。

「黑色的布拉甲……」

「威∼爾……」

「不，這次不是胖次了……」

「但是啊，那是布拉甲不是嗎！」 (ブラジャー這邊這樣翻比較有實感)

確實伊娜說的對，想到陷入了最壞的狀況裡面了。

連續召喚來的都是內衣，我的品行也會遭受質疑。

以前沒有被人這麼說，到那時就不一定了。

「是誰的呢？」


「嗯，尺寸是S」

另外碧根鮑華氏開始目不轉睛的觀察起拿過來的黑色布拉甲。

可以說，這個是研究者以認真的態度觀察被召喚的物品。

外面看來，就是個變態老頭而已。

「威∼爾ーーー！」

然後，過了約幾分鐘後。

這次，換成露易絲闖進了研究室內。

果然還是那位女性職員親切的引導下到來。

「哎！ 露易絲的嗎？」

沒想到，那個露易絲是穿黑色內衣。

稍微有些話想說，如果說出來大概會變成不得了的大事，所以我還是不要說的好。

只是，碧根鮑華氏好像讀不懂空氣。

「那個幼兒體型，穿黑色的內衣還太早了吧。而且，想一想似乎也不用穿布拉甲……」

「哼！」

「嗚哇！」(あべし 這句是北鬥神拳的NETA)

碧根鮑華氏被露易絲左右來回的巴掌伺候，臉頰變成紅通通像是楓葉一般的顏色。

「那個，下次一起去買內衣吧」

「呼ーーー嗯，好吧」

「小子，被迷上了真是太棒了」

再次逃過免於被露易絲制裁，我又能放心下來了。

「為什麼老是內褲呀」

「知道嗎。影像的調整是困難的喲」

「威爾也好不到哪裡去，可以跟那老頭一樣當成變態了」

「稱呼魔導工會的研究部長是變態老頭……」

「這種狀況實在很難讓人否定」

這次連露易絲也加入了，果然實驗還是要繼續進行下去的樣子。

碧根鮑華氏的兩頰疊了兩層楓紅一般的印子，要我把下次召喚的東西說出來。

「屋子外面的會比較好嗎」

「但是，那樣子的話就麻煩了」

「但我(私)在屋子裡面胖次被搶走了」

「我(僕)，胸罩(可不想)」 (原文:ボクは、ブラを)

伊娜和露易絲向這邊投射責難般的視線過來，我下定決心要快快結束這個無聊的實驗

「（儘量是距離越遠的東西就好了）」

另外，沒有必要勉強召喚能夠成功。

從遠距離召喚過來，只要消耗所有的魔力就結束了。

在這種情況下鄰近的地方就是最好的結果。

順便說一下，從地球召喚物品在這個時間點上很危險就不做這樣的打算。

我平時就摸索安全的策略，因此那種情況隨波逐流的男人也很多。

「耶多……。目標是亞卡特神聖帝國的領土」

「原來如此，外國的話就不會被抱怨了呢」

伊娜隨口說出她的欽佩，真不愧是從住在那個地方的人進行召喚的話會是嚴峻的。

於是，看了參考前幾天前得到的的地圖，試著嘗試召喚不知何人的所有物的不自然物品。

「（亞卡特神聖帝國北方的海域……）」

亞卡特神聖帝國擁有整個琳蓋亞大陸的北半邊作為領土。

然後大陸的北端與南端都同樣是廣大的海洋與島嶼遍佈。

並且，冬天當寒冷的時候那片海洋捕撈到的海產品在地國中很受歡迎。

以前讀過的書本中，書裡描述的那個產物酷似地球內的北海道可以採集的產物。

之後……。

「（其實這個魔法陣是很方便的東西？

嘛∼這些等成功了再來說……）」

一邊這麼想一邊站在魔法陣前面，接著青白色的光芒後從那上面出現了不知什麼的東西。

「內褲？」

「不是那個東西啦！」

我一邊對伊娜吐槽一邊急忙跟魔法陣上頭出現的物體保持著距離。

「成功了。但是……」

魔法陣的上面出現的東西。

那是，地球上被稱之為黑鮪魚的魚類。

而且，還很客氣地只召喚在海中游泳重量接近二百公斤的個體。

還活著的黑鮪魚，還很有精神的在魔法陣上面來回跳動著。

「伊娜」

「沒辦法呢……」

伊娜從我在魔法袋取出的槍拿過去，快速的刺向鮪魚讓它停下來。

很快的鮪魚就在魔法陣上面不動了。

「原來如此，這次很快地就完成召喚出有用的東西了啊」

「不，碧根鮑華先生還敢這麼說嗎？」

說真的，最初將胖次召喚時就已經不想多說什麼了。

「嘛∼不是幹得不錯嗎。快點來吃了它吧」

「可以吃的啊！」

其實，琳蓋亞大陸魚類生食是很普遍的。

放入魔法袋內的話鮮度就不會下降，內陸地區的王都與附屬都市的話，只有有錢人與王侯貴族才能好好地品嚐這種高級食材。

只是，芥末是西洋芥末的近似物，但並不是用醬油而是沾著鹽一起吃。

「果然，新鮮的魚最好吃了」

那種程度的大鮪魚吃了吃後的想說的就是這樣。

如果不能以專業的手法來支解會很麻煩，但是那個煩惱被先前從碧根鮑華氏那邊搶走胖次的女性職員手上解決了。

事前並不知道她的老家是開魚店的。

從老家借來的工具，熟練的手法將鮪魚支解了。 (マグロ，這個除了當作金槍魚或鮪魚，放在地下鐵或是捷運有另外的解釋...)

然後，用高手般的分切方式將生魚片裝盤了。

「北方產的黑鮪魚重量是二百十三公斤。行情是二十萬銅左右呢」

突然出現的高級食材，位在魔導工會的全員都聚集過來一起吃生魚片。

「哎，是那樣嗎？」

「是的，赫爾姆特王國沿岸雖然也能捕獲，但是味道上還是北方產的比較好」

北方產的黑鮪魚，日本所說的能匹敵品牌大間的所產鮪魚。

更進一步的說，進口產品價格很高昂。

「原來如此。這還真是好吃」

前世，還不至於連這種高級鮪魚都沒吃過，指示還是被那美味所感動了。

還有，為了吃生魚片所提供出來自製醬油也頗受好評。

「比沾著鹽吃，這邊的更加美味呢」

「這樣的話，接下來的獵物就更想要了」

到現在為止都只是召喚出胖次的魔法陣，第一次有人召喚出有用的東西。

這裡持續注入魔力，持續全力進行召喚吧。

「北方之海的美味食物」

「期待……」

我一邊不斷讓吃驚的伊娜冷靜下來，一邊不斷召喚北方產的魚貝類。

有比地球更大的扇貝，還有近似馬糞海膽的海膽。

烏賊與章魚都是高級食材，所以大家都很高興地吃著。

好歹，沒有在做出只有惡魔才會使用的做法。


居住在琳蓋亞大陸的人們，比起平常吃動物的肉從海裡獲得的海產物是高級品給人美味的印象更為強烈。

所以，突然開始的試吃大會非常受歡迎。

「繼續！」

這次是真鯛、比目魚、鰈魚、鰤魚等。

正式名稱是不同的，但那個魚店出身的女性職員看到外表說那也是高級品也很好吃所以就沒有問題了。

她，不斷地將召喚出來的海產品分切成，那些魔導工會的職員們不斷地也將東西送入口中。

我也同樣因隔了好久的海鮮食品讓舌頭都打起鼓來了。

「那麼。差不多大家都吃飽了」

「不，跟那個沒關係。這可以說是實驗！」

「那個我是知道的喔」

不愧是我的(龐大)魔力量，還能再一次召喚就到達極限了。

放眼四周，都是吃光生魚片得到滿足的職員們，就不在意那些繼續進行最後的召喚。

只是，剩餘的魔力不多的關係，太沉重的物品就無法召喚了。

「希望把距離當作重點來設置目標」

「我知道了」

如碧根鮑華氏說的，我又從北方的海域得到很輕的物品的影像。

結果成功率是１００％，魔法陣的上面發光的同時不知什麼樣的小東西被放在那邊。(這邊開始就算是78-79話的伏筆，本人還沒看到那邊稍微看79話的劇情泰蕾莎似乎非常針對威爾而來)

「紫色的……」

「內衣呢」

「威爾！」

「為什麼又是內衣啊！」(註:之前的下著都是內褲而已，這裡是一整套所以用不同寫法)

「不知道！」

以北方之海的海產為目標，可是我又把內褲召喚過來了。

被伊娜與露易絲譴責了，另外我並不是喜歡內褲才召換過來的。

「為什麼？」

「大概不知道誰在海上乘船的可能性很高。只是，這件內褲……」

三次，碧根鮑華氏開始將手裡拿著的內褲進行調查，不僅僅是伊娜、露易絲還有上述的女性職員。

連魔導工會的會長都用驚訝的表情看著碧根鮑華氏。

「顏色是紫色，材質是絲綢。尺寸相當有份量的胸罩，蕾絲與薄紗被大量地使用。縫製用一句話形容就是太完美啦」

「変態……」

伊娜說的沒錯，本來魔法的研究家碧根鮑華氏對內衣這麼熟知心情上很微妙。

但是，碧根鮑華氏說那些又不是奇怪的事情。

「老夫的老家是貴族御用的內衣專門服飾店。自然有內衣的相關知識」

「那些知識，有辦法自然到就學會了？」

「住在老家的時候，備辦強迫幫忙家裡的事業」

魔法使的才能事不能遺傳的。

因此，各種各樣的階級和做生意的家庭出然出現了這種人才，因此那個也能說是悲劇。

然後，碧根鮑華氏把那微妙的過去吹散到地平線的彼端一樣，弄清楚衝擊性的事實。

「這個家紋是……」

「哎？ 內衣還有家紋嗎？」

碧根鮑華氏確認那件內衣附有家紋。

「王國也一樣，王族與皇族，不是一般普通指定的御用品而已。、指定された御用達の品しか身に付けないのが普通。跟店家那邊其他的產品為了區隔會以刺繡繡上家紋來做區分」

真不愧是碧根鮑華氏比起已經是魔法的研究家，對於內衣的專業更讓人有強烈的印象。

還能適當地深入的解說。

「順便一說那個家紋」

「嗯，是菲力浦公爵家的東西。同時也是選帝侯，亞卡特神聖帝國數一數二的大貴族家」

世上的人會認為知道越少才越幸福。

或者說，這樣的大貴族家的女性內衣被奪走了，弄個不好就會發展成外交問題吧。

包含我在內，所有人臉上全都發青。

「那個，布蘭塔庫先生」

「別問我喔」

「碧根鮑華先生？」

從布蘭塔庫先生的話，確實不知道該怎麼回答比較好。

因此，詢問這個實驗的負責人碧根鮑華氏，不過他的回答快到就像反射神經一樣。

「北方之海的海產召喚平安無事的成功了。內衣？那是什麼東西完全不知道。是這樣子的吧？鮑威斯麥男爵殿下」

「是的，什麼都不知道！」

我很快地把內衣收進魔法袋中後完成了工作。(還是要說這是Flag)

這樣就完全湮滅證據了。

此刻，某位菲力浦公爵家的女性現在是No-pan・No-bra的狀態，那樣子我們是無法干預的。

「這樣真的好嗎？」

「不太好吧，老實向陛下說如何？」

「這件事無法言明……」

對我的疑問，伊娜也決定閉口不談。

會長以下，其他工會職員們被下達封口令，只有留下公式性的紀錄說北方的海產品召喚成功了。(這是第3次的Flag)

只是，後來與那套內衣的主人牽扯上大事件，連我都無法想像。

然後第二天……。

「我回來了，爸爸」

「歡迎回來、迪莉亞。對了，有一件事情想問你」

案例中那件白色胖次的持有人，魚店的千金兼魔導工會職員的迪莉亞，今天工作完後回到家裡，接受身為魚店老闆的父親的質問。

「什麼事？ 爸爸」

「今天，從鮑威斯麥男爵家送來大量的訂單。你，知道原因嗎？」

「大概是得到爸爸真傳的手藝被認可的關係喔」

「蛤啊？」

後年，迪莉亞小姐是鮑威斯麥領經營魚店掛上從王都的本店拿到的門簾作為分店的招牌。 (暖簾，這東西看深夜食堂那個老闆開店會掛上的那個東西)
