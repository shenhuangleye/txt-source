４

到了第三天早上時，奧爾斯特如此說道。

「很抱歉，今晚也麻煩各位野營了。明天中午前會進入城鎮，到時請買足食物。蓋托先生，請大量購買馬匹的飼料。」

雖然不知道奧爾斯特是以怎樣的理由來判斷，但這是委託方的指示，沒有人會有異議。
之後，多次與馬車和旅人擦身而過，但沒有特別可疑的對象。

剛過中午時，〈生命感知〉發現了什麼。

「凡達姆。前方左側的山丘另一面，有誰躲著。一個人。」
「嗚。側面沒防備的時候被從上方遠距離攻擊可受不了阿。」

凡達姆摸著鬍子，稍作思考。

「到山丘上面的距離，約百步嗎。從上方的話，箭能充分射下來。雖然箭對馬車沒什麼效，但要是恩寵武器就很麻煩阿。」

艾達插了嘴。

「山丘上的人露臉後，如果打算攻擊的話，我就射魔箭過去。」
「好，就這樣辦。各位！前方左側山丘有可疑人士！一邊警戒一邊前進！要是攻擊過來就反擊！」

此時，凡達姆因為對方只有一人而輕忽了。認為只要有這一行人的攻擊力，就無需害怕。很快地，就會對這判斷感到後悔。
艾達爬到了馬車上。在搖搖晃晃的馬車上單膝跪地取得平衡，已經舉好弓生成了魔法矢。是隨時都能攻擊的態勢。
雷肯用〈生命感知〉探尋對方的位置，把自己放在對方和馬車之間。馬車來到山丘正旁邊時，山丘上有人遲鈍地站起。

看到了那姿態，凡達姆叫出悲鳴。

「艾達！」

艾達射出了箭。箭筆直地飛向山丘上的襲擊者，刺進胸口。然而襲擊者別說倒下了，連感到痛楚的樣子都沒有。

是個奇異的男人。
上半身全裸，只穿著小小的背心。下半身穿著鬆垮的褲子。胖得難以置信，顏面和身體都黝黑發亮。明明有著這樣的距離，但感覺得出他兩眼正充血著。

凡達姆開始策馬爬上斜坡。不過，為了不擋到艾達的射線，有稍微往右繞。

山丘上的男人從放在腳邊的袋子裡，用右手取出一個黑色的小東西，往上拋出。接著用兩手握住原本握在左手上的巨大槌子，用力一揮。

此時，艾達的第二擊命中了男人的腹部，但果然沒有任何效果。

揮出的槌子打中黑色的小小球狀物時，劇烈的爆炸發生，黑色物體接著向馬車高速飛去。
憑雷肯的視力和反射神經，要閃過這飛來的物體很簡單。但是閃過的話，說不定會擊中馬車或艾達。

「〈炎槍〉！」

這是近乎無意識的攻擊。發動比至今使出的〈炎槍〉都還要快。在連眨眼都不夠的細微時間內，魔力順利地循環收束，魔法被正確地建構而出，高威力的攻擊魔法現出，與相距馬車二十步左右的黑色砲彈衝突。

轟音爆發，黑色砲彈爆裂了。

「〈炎槍〉！」

接著發出的魔法攻擊，以異樣的速度擊中了敵人。遠比艾達射出的魔法矢還快。在後方騎在馬上的澤奇眼裡，只看到光的直線連接在敵人和雷肯的手之間。

炎之槍在敵人的胸口開了個大洞。敵人接著慢慢倒下。
不知何時，妮姫到了雷肯旁邊。劍有被拔出來。說不定是打算擊落那個黑色砲彈。


５

「是叫〈破碎槌的布夫茲〉的冒険者。原本是迷宮專門，在夥伴死去之後，改接見不得人的高額工作。應該有被好幾個城鎮通緝。」

根據凡達姆的說明，布夫茲的恩寵品槌子，似乎會在打中東西時爆炸。就是以那反作用力來擊出鐵砲彈。

「抱歉。是我的判斷失誤。別說先發制人攻擊了，至少也該停下馬車偵察才對。浪費了雷肯難得的情報。」
「沒有浪費。」
「總之，你幫了大忙。謝了。」
「雷肯。」

旁邊搭話而來的是，魔法使澤奇。這個寡言的男人會搭話還真是稀奇。

「剛剛的〈炎槍〉，沒有準備詠唱。第一發可能是私下先準備的，但沒有能準備第二發的時間。是怎麼做到的。」

正思索著該怎麼回答，凡達姆便插了嘴。

「澤奇。冒険者沒可能隨便暴露本領的。別再問了。」
「嗚。」
「不好意思。」

出聲的是奧爾斯特。

「在這種場合下，戰利品的權利會屬於護衛全體。這次的場合是恩寵品的槌子。有必要判斷，是要保留實物，還是要賣掉換錢。」
「雷肯。你想要這個槌子嗎？」
「不了，用不著。」
「是嗎。那就賣掉吧。雖然對不起你，但要讓護衛全員均分。」
「啊啊。」
「那麼，很抱歉要麻煩各位，因為我想實際確認威力，雷肯先生，能請您用這槌子攻擊那棵樹嗎。」

雷肯舉起槌子，走向指示的樹。

不可思議的槌子。原以為是金屬，但總感覺是木製的。戰鎚的打擊面通常不怎麼大才對，但這槌子宛如木槌，兩邊都又平又大。

揮出槌子，打擊樹木的側面。
發出了劇烈爆炸。槌子差點被吹飛出去，雷肯反射性握緊雙手。

粗樹幹斷了一大截，連著枝葉的上半部倒了下來，發出巨響。

「真是出色的威力呢。也能麻煩凡達姆先生試試看嗎。」
「知道了。」

凡達姆從雷肯手上接下槌子，但不自覺地放了開來。

「這什麼重量！」

凡達姆伸手撿起槌子，很了不起地，使力舉起槌子，往附近的樹砸去。
爆炸發出，槌子雖然被反作用力吹飛，但樹木還是折斷倒下了。

「非常感謝兩位。威力和問題點都大致了解了。要拍賣的話會花上不少時間。如果現在賣給本店的話，能以四枚金幣買下，各位意下如何呢？」

凡達姆一臉意外地看向雷肯。

「凡達姆。你來決定。」
「抱歉阿，雷肯。奧爾斯特先生，麻煩你買下了。」
「感謝您的惠顧。」

一人八枚大銀幣的臨時收入。凡達姆跟澤奇都很高興的樣子。