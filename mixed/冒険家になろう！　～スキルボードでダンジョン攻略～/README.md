# novel

- title: 冒険家になろう！　～スキルボードでダンジョン攻略～
- title_zh: 成為冒險家吧！　～用屬性面板攻略地下城～
- author: 萩鵜アキ
- illust: 栗山廉士
- source: http://ncode.syosetu.com/n8618ef/
- cover: https://images-na.ssl-images-amazon.com/images/I/91j3jkChDFL.jpg
- publisher: syosetu
- date: 2019-08-30T18:00:00+08:00
- status: 連載
- novel_status: 0x0300

## illusts

- ＴＥＤＤＹ

## publishers

- syosetu

## series

- name: 冒険家になろう！　～スキルボードでダンジョン攻略～(WEB連載版)

## preface


```
存在感稀薄的冒險家空星晴輝在離開出現在札幌的『地下幌』的地下城回家時，家裡的車庫變成了地下城。

在地下城的入口，發現了像石闆一樣的東西。
難道這是技能面板？

晴輝利用技能面板前往地下城。

利用技能面板，終有一天成為像排位者一樣引人注目的情況！
――但是，為什麼會變成戴著假面背著土豆的怪人？！

他能作為『正經』的人成功攻略地下城嗎？

---

　存在感の薄い冒険家、空星晴輝が札幌の『ちかほ』に出来たダンジョンから帰宅すると、家の車庫がダンジョンに変化していた。

　ダンジョンの入り口に、石板のようなものを発見。
　もしかしてこれは、スキルボードか？

　晴輝はスキルボードを用いてダンジョンへ。

　ボードを駆使し、いずれランカーになって目立つ存在になる！
　――はずが、何故か仮面を被りジャガイモを背負った不審者に?!

　果たして彼は『マトモ』な人物としてダンジョンを攻略出来るのか？

※本作は実際の地球・日本・企業・団体・個人とは一切関係のない『フィクション』です。
```

## tags

- node-novel
- R15
- syosetu
- そのうち最強
- スキルツリー
- ダンジョン
- チート
- テイム
- ネット小説大賞六
- ファンタジー
- ローファンタジー
- ローファンタジー〔ファンタジー〕
- 冒険
- 変態
- 残酷な描写あり
- 災害
- 現実世界
- 迷宮
- 現代
- 現代異界化
- 現代ファンタジー
- 異界化
- 

# contribute

- 列斯兰卡

# options

## downloadOptions

- noFilePadend: true
- filePrefixMode: 1
- startIndex: 1

## syosetu

- txtdownload_id:
- series_id:
- novel_id: n8618ef

## textlayout

- allow_lf2: true

# link

- [narou.nar.jp](https://narou.nar.jp/search.php?text=n8618ef&novel=all&genre=all&new_genre=all&length=0&down=0&up=100) - 小説家になろう　更新情報検索
- [小説情報](https://ncode.syosetu.com/novelview/infotop/ncode/n8618ef/)
- https://www.lightnovel.cn/thread-971303-1-1.html
- http://seiga.nicovideo.jp/comic/37022
- https://www.dm5.com/manhua-chengweimaoxianjiaba--yongjinengmianbangongluedixiacheng/
- https://bookwalker.jp/de917038cd-bd00-4542-ae7d-a06e66f9c937/
- https://neo.usachannel.info/2019/03/15/post-3838/
- 


