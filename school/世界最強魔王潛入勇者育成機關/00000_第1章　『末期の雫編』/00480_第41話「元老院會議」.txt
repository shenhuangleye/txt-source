艾伯利亞帝國的首都亞古雷亞。

在正好位於南方領的米盧迪亞納北方的那個大都市的一角，有為了進行元老院會議的豪奢的設施。
張開了好幾重的魔術結界，施行著即使有誰襲來也連傷害沒有戰力的元老院議員都做不到的警備體制。

集中在那裡的元老院議員們大約20人不到。
無論哪一個都淨是以公爵家為首的名門家世的人。
他們一致顯出不高興的樣子在長桌列席。

對此毫不在乎，帝國軍米盧迪亞納領總司令官琉迪奧·蘭伯特開口道。

「誒—。這次各位在百忙之中匯聚到此，我深表感謝」
「這是怎麼一回事，蘭伯特中將。元老院會議的預定最晚應該在1個月前通知吧。僅僅7天就要召開會議太超出常規了。那個長耳朵已經夠搞笑了」

下賤的笑聲響起，但琉迪奧以爽快的表情說。

「非常抱歉。可是此次正如書信中所寫，是緊急的會議。務請寬恕」
「哼，就是米盧迪亞納領被怪物們襲擊了的那件事嗎」
「不錯。雖然大部分人已經知道了吧，但請讓我說明一下事情的原委」

天魔襲擊米盧迪亞納領事件之後約半個月後。
琉迪奧只集中了在帝國之中也是最高位的元老院議員，召開了緊急的會議。

本來的話這個場合艾伯利亞帝國的皇帝和宰相也會參加，但因為皇帝的身體狀況不佳而且有緊急的會議不方便所以不在場。
然後琉迪奧講完在帝國發生的森精靈失踪事件的始末後，立刻就有奚落聲飛來了。

「因為那個天魔什麼的襲擊好像出現了很多犧牲者吶？ 米盧迪亞納軍的力量也一落千丈了哦」

沒錯的話語持續之中，坐在無腿靠椅上的1位森精靈的女性開口了。

「正是如此。米盧迪亞納軍真的是沒用。要說為什麼的話，因為像汝們一樣蔑視森精靈的人很多。無非是反森精靈主義者飛揚跋扈的結果」

對杰弗泰·亞里亞王國的艾茵拉娜·吉爾菲尼斯卡女王說的話，貴族們發出了反感之聲。

「蔑視森精靈？ 哪裡的話。我們已經是同盟關係了吧」
「是啊。給森精靈爵位，給森精靈將校之位，廢止森精靈的奴隷——我自負對森精靈來說是無微不至的優遇」

盯著露出了微笑的貴族們的艾茵拉娜的眼睛像冰一樣冷。
她看透了眼前的人物的內心。只在森精靈的王族中傳承的像特性一樣的東西，在貴族之中也有很多人知道那個。
但是，她的那個已經不是察知真心話這樣的範疇了。能夠鮮明地理解那個人在想什麼。

「的確是被優遇了吶。但是，因此汝們的怨恨層出不絕。這樣下去早晚同盟關係會開綻也不無道理」

對懷疑聽錯了的發言，元老院議員們激怒了。

「你，你說什麼！？ 你不知道我們對森精靈做出了多大讓步！！」
「你說怨恨！？ 胡說八道！ 這種明顯的優遇制度就是在侮辱我們人類的驕傲！ 這個艾伯利亞帝國是屬於我們人類的，不是你們的！！」

差不多應該制止了吧。
琉迪奧正要開口時，會議室的最裡面。沒有坐在椅子上，而是背靠著牆站著的高個子的男人說道。

「安靜」

因為響徹心底般的有威嚴的話，誰都將目光轉向了那邊。
元老院議員們異口同聲地說。

「克羅德·杜拉斯將軍……！」
「戰爭的大英雄……！」

被稱為克羅德的那個男人穿著也是艾伯利亞帝國軍的證明的黑色鎧甲。
金髮碧眼。年齡不滿30。簡直就像把雕刻就那樣變成了人一樣端正的容貌。

可是，還不失年輕的那張臉近于無表情，充滿了難以接近的氣氛。
他將銳利的瞳孔轉向了艾茵拉娜。

「艾茵拉娜·吉爾菲尼斯卡女王陛下。此次的議題並非差別問題的追究。多餘的發言會妨礙會議。請嚴謹」
「……說得也是呢。失禮了」

「蘭伯特中將。重開原委的說明吧」
「是」

琉迪奧立即回應。
全部說明完事件的概要時，元老院議員誰的臉上都染上了驚愕之色。

「——在高等魔法院的地下發現了有著綠色體表的奇怪的生物。還生存著的只有2只。正如之前說明的，這個生物造成了末期之雫」
「在高等魔法院竟有這種事……！？」

「一下子難以相信！ 讓我們看看那個生物什麼的吧！」
「辦不到。體長近5米。要怎樣運到這個地方好呢。本來就是危險的怪物」

元老院議員們誰都是難以置信的樣子並顯示了動搖。
也難怪吧。在這裡的人大部分恐怕連米菈的血潮事件都不知道。那個事件是被隱藏到這種程度被淡忘了的事件。
成為事件淡忘的『最主要原因』的艾茵拉娜補充說。

「妾身也在事件之後在跟前看了那個生物。對人類沒有興趣，但見到森精靈的妾身的瞬間顯示出了想捕食妾身的動作哦。雖然因為在魔法陣上動不了，所以什麼事都沒發生」
「那，那個生物到底是什麼！？ 大概不能認為是普通的生物啊！」

「那是妾身想問的。但是，如果推測正確的話恐怕是在那個魔術大國齊亞洛·迪爾納王國被製造的魔導生物吧。『只為了排出末期之雫』而活著的那個姿態正是生物兵器」
「魔導生物……！ 帝國已經沒有引進那些生物了……」

議員的1人嘟噥後，琉迪奧也點頭了。

「誒誒。魔導生物至少這300年以上沒有被輸入到帝國過。而且在製造那個的過程中會進行各種各樣的非人道行為。雖然和那個國家的邦交也已經很少了，條文正是形式化了，但是300年前當時的皇帝陛下明令禁止輸入呢」

「那麼，那種生物是偷偷被帶進來的嗎。過去有擁有同樣特徵的魔導生物的輸入記錄嗎？」
「我被配屬到軍部後，調查過過去的各種各樣的文獻。當然，也把握了過去和魔術大國之間『正式』被交易的魔導生物的清單，但是不存在擁有那種特徵的東西」

琉迪奧又繼續。

「那個米莉亞姆·斯泰西斯少尉被這次被視為幕後黑手的吉斯蘭憑依了，但無法確認她本身和魔術大國的聯繫。但是，這次的森精靈失踪事件在米盧迪亞納以外也發生了。也就是說，有那個魔導生物在其他領地也被偷偷飼養著的可能性」
「怎，怎麼可能！！」

「你是說現在也仍然有和魔術大國有很深關係的人嗎！？ 那個失踪事件何時在何地發生著？徹底調查！」
「我當然打算那樣做，但預計會花上相當久的時間」

艾茵拉娜再叮囑。

「現在貴國正在準備支援盧加爾王國吶。聽說北方和東方的2大軍隊會全體出動。妾身不覺得短期之內能進行正經的調查」

元老院議員們馬上回答。

「獸人們的對手也是齊亞洛·迪爾納王國吶……。如果這次的事件也和那個國家有關係的話，更加要對盧加爾進行援助」
「帝國的調查自不必說，盡可能快地介入2國間的戰爭迅速制壓齊亞洛·迪爾納王國。之後徹底調查那個吉斯蘭什麼的不就好了？」
「唔姆，的確如此。特別這次是聚齊了北方和東方的2大元帥的重兵。雖說在和瑟南的戰爭中疲敝了，但如果是攻陷齊亞洛程度的話應該馬上就能做到」

結果偷換成了戰爭的話題嗎。艾茵拉娜一副想那樣說的表情挽著胳膊嘆氣了。
艾伯利亞帝國援助盧加爾王國的理由雖然是舉著保護被虐待的獸人——這一大義名分，但實際上深深地牽涉到利權。
元老院議員關心的事早就已經從終焉的森精靈們的失踪變化為了下一次戰爭。

不如說，關於高等魔法院的醜聞是禁忌。如果不小心說了什麼的話不知道自己的腦袋會變成什麼樣。
關於那個機關就是如此高機密性、不可侵犯。不僅是軍部，此外如果沒有元老院以及皇帝的許可的話，無論有怎樣的理由想要接觸那個機密都是不被允許的。
即使是軍部的總司令官。

這時，默默在聽的克羅德·杜拉斯將軍說。

「蘭伯特中將。我想問幾個問題」
「是。請隨便問」

「此次的森精靈失踪事件可以說終結了嗎」
「恐怕是的。雖然即使考慮了同樣的事之輩再次出現也不奇怪，但考慮到現在的情勢的話那個魔術大國會把擁有和吉斯蘭同等力量的魔術師送來帝國的理由很少。因此，認為暫時什麼都不會發生也沒關係吧」

「關於高等魔法院，你怎麼想」
「……必須盡快請示陛下，盡可能快地停止內部的腐敗。雖然這終歸是我個人的意見」

面對不滿自己一半年齡的對象，琉迪奧稍微被氣壓了。
在之前與瑟南龍王國的戰爭中包括自己在內有5個被稱為英雄的人們。

雖然在那之中也是最年少但不許其他人追隨，單獨斬入最前線未嘗一敗，以眾多強大的龍族和龍人們為對手未負一處擦傷的戰爭的救世主。
而且能揮動被認為是創造的大女神奧爾菲莉婭打造的『神劍』的唯一被選中之人。

因而被稱為大英雄的男人，那就是克羅德·杜拉斯將軍這個人。
公爵位，軍銜是在那個年齡破例的大將。再稍微過幾年肯定會到達元帥的地位吧。

在琉迪奧所知中也毫無疑問是最強的人物。
豈止是歴戰的猛者。已經匹敵戰神的他的目光之銳利連琉迪奧都不禁顫慄。

那當然不僅是琉迪奧。
在場不可能有人面對克羅德的目光不感到恐懼心理。
誰都在畏懼應該不過是年輕人的他的發言，身體僵硬了。

「那麼，最後一問。在場有和那個叫吉斯蘭之輩共謀的人嗎」

當場一口氣人聲嘈雜了。
克羅德不改無表情，狠瞪著琉迪奧。

米盧迪亞納領總司令官儘管感到了緊張感，還是向森精靈的女王使了眼色。
看到她聳了聳肩之後，他慢慢地搖頭了。

「雖然不至於說能完全否定，但我想是沒有的」
「是嗎。我沒有要說的了」

只告知了那個後，克羅德又閉上了嘴。
那之後元老院會議肅穆地進行，閉幕了。

---

帝都亞古雷亞的元老院會議場。

在已經沒有了人影的會場的玄關，那個男人站著。
他一注意到這邊就甚至沒對好久沒見面的友人露出笑容地說道。

「琉迪奧。繼續在元老院會議的時候不能說的話」
「……你的急性子就不能想想辦法嗎，克羅德。偶爾要給點時間談笑呢」
「時間很珍惜。作為就任西方的總司令官的人，我也不能在這個地方呆太久」

在公共的場合以外，互相共同不以姓而是以名稱呼。
琉迪奧和克羅德是故知的關係。是戰友甚至能說是親友吧。
看著那樣的2人，森精靈的現女王笑了。

「雖然聽說是琉迪奧的親友，但真是正經古板吶。汝也不要老是傻笑了，稍微學學這位大英雄殿下怎麼樣？」
「我想至少要和藹可親。因為本來就是被討厭著的身份呢」

琉迪奧苦笑著決定進入正題。

「是吉斯蘭和在背後的人的話題，呢」
「對。如果和你說的事實對照的話，即使在帝國軍內部有協力者，吉斯蘭什麼的單獨犯罪也是不可能的是不用說的。說到底，你在那個地方揭露到了哪種程度？還有沒說的事吧？」

艾茵拉娜沒有插嘴注視著事態的發展。
——100年前艾伯利亞帝國與杰弗泰·亞里亞王國的同盟關係締結時。
有件表面上完全沒有被發表的事。

那就是『在艾伯利亞帝國與杰弗泰·亞里亞王國的兩國，廢棄關於米菈的血潮事件的全部記錄』。
在100年前的時間點大部分人連那個存在都不知道的事件為何需要必要以上的隱匿呢。

因為帝國與杰弗泰·亞里亞的同盟締結連接著人類與森精靈的和睦。
為了哪怕是一點也要拉近兩者的關係，成為親愛的鄰居，只有被公眾知道『如果進行使用森精靈的儀式的話，就能造出能得到精度極高的魔力增幅效果的什麼』這一事實是不管怎樣都必須避免的。

第一次聽到這件事時，琉迪奧憤怒了。
因為連幕後黑手的正面目都沒能抓住，還要進行發生過的事件本身的隱蔽的話就是捨棄了什麼人再次干同樣的事的可能性。
然後沒想到，那件如果是杞憂就好了的事由某人的手變成了現實。琉迪奧告知了那個。

「被稱作『女神』的什麼參與了事件」
「那是什麼。什麼的比喻嗎？」
「不知道。所以，在元老院會議的時候我控制了發言。因為不能說多餘的話讓腦袋裡只有自私自利的老人們的腦子混亂呢」

「……吉爾菲尼斯卡女王陛下，您對那個女神什麼的有什麼線索嗎」
「不清楚啊。500年前不論多麼徹底的調查都一無所知，這次本以為終於能抓住幕後黑手的尾巴了卻就這樣尾巴斷了」

「那麼，那個女神什麼的情報你是在哪裡獲得的，琉迪奧」
「那是精神崩壞的吉斯蘭屢次說出的話。他固執於女神。……然後」

琉迪奧繼續告知。

「這也是為此次查明失踪事件盡了力的可靠的我軍校的學生帶來的情報。特別是——如果沒有『他』的話，這會兒我已經不在這個世上了吧」
「那是什麼人」

「今年剛剛完成軍校入學的少年。名叫提奧道爾。是在軍校全部3種入學考試上成為首席，之後瞬間解明了我只給特待生布置的特別考試的逸材」
「那是個非常優秀的男人啊。雖然妾身沒有對帝國軍校的歷史熟悉到那種程度，但看到被認為擁有比得上500年前勇者的本領的他和吉斯蘭講話後妾身深切地感到。歷史上沒有和那個並立的人吧」

艾茵拉娜言外之意是說在500年前的勇者之上。

「正如您所說。有他在幫大忙了」
「那個男人是什麼人。擁有那樣的本領的話，不是從入學以前就會被另眼相看嗎」
「根據調查，看起來是從帝國的西方領——而且，是從你守護的格蘭登來的哦？」

對像挖苦一樣說的琉迪奧，克羅德皺起了眉頭。

「別胡說八道。有那樣的人的話一定會成為傳聞。別說我，不可能不進入軍部的耳中」
「是吧。哎呀真是的，只能說是不可思議的少年了。而且他說過很有意思的話。他說『怎樣才能成為勇者呢？』」

「……勇者？」
「欸欸。最後從這個帝國前去殲滅戴涅布萊的魔族的勇者是500年前的蕾娜大人。她最後沒有回來。然後從戴涅布萊發動戰爭的事也沒有了——如今連知道勇者被好幾次派遣到那個國家，全部都沒能歸來的人都沒了」

愉快地告知的琉迪奧又繼續說。

「而且，他也這樣說過。對魔族沒有仇恨嗎。當然其他學生的反應很小，知道這個的他是非常失望的樣子哦。簡直就像想說人類恨魔族是理所當然的一樣」
「庫呋呋！ 這簡直就像至今為止一直被軟禁在哪裡，最近第一次到外面來後知道了現實的樣子吶。實在有趣」

艾茵拉娜也愉快地笑了。

「……戴涅布萊魔族國嗎」

那樣嘟噥後，克羅德眺望了遙遠西方的土地。

「克羅德，關於那個國家你什麼都不知道吧？」
「啊啊。在國境附近發生小衝突也是幾十年前的事了。之後一次也沒有陷入那種事態。然而我一直這樣在想。不是在魔族之中也相當於最上位的被稱為魔王的人們現在也仍然活著，以強大的統率力嚴格約束著眷屬們嗎」

「關於魔族的事我也只有在文獻中得到的知識。最多也不過是幾百年前的東西。據說魔王——最上位的魔神們是從比那個更遙遠的過去，從這個艾伯利亞帝國建國以前就存在了。真是荒唐的話啊」
「此次的失踪事件，或許也不能捨棄和魔族有關的可能性吧」

琉迪奧沒有否定。或者說是無法否定。
戴涅布萊魔族國是擁有和這個帝國同等面積的大國，大部分的魔族們在那裡生活著。

然後位於那個頂點的被稱為魔王的存在有7柱，他們有著非常強大的力量——搜集再多文獻出來的情報都只有這種程度。
不知道他們對其他種族抱著怎樣的感情。其中好像也有擁有特別凶惡的思想的魔神，但詳細的什麼都不知道。

「關於魔族，妾身覺得不用太在意吶」

突然那樣說的是艾茵拉娜。

「吉爾菲尼斯卡陛下。這是有什麼根據的發言嗎？」
「呋姆……根據嗎。特別的是沒有。就是妾身的直覺」

雖然是留有餘地的說法，但金髮的將軍沒有再過多提及。

「女王陛下的直覺之強不可小看呢。這次就先希望猜中了吧。因為鑑於現在帝國的情勢，沒有甚至於對現在連如何生活著都不知道的魔族都維持警戒的餘裕」

那之後，2位將校與森精靈的女王交談了片刻後就解散了。
