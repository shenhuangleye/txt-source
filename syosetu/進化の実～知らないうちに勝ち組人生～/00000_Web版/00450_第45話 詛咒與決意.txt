一個人被留在訓練場的俺，發動了剛剛入手的的技能『世界眼』。
這個技能，處於時常發動狀態的時候，好像是只有『心眼』的效果，要憑意識去發動才有『索敵』的效果。
發動技能，姑且還是探索了一下從這個王城往外逃離的存在。
然後，表示在王城內部的露易絲小姐她們的是，在腦中出現的如同雷達一樣的畫面上所顯示著的青色的點，與之相反，被雷達檢索出來的以驚人的速度逃離王城的存在，則以紅色的點表示出來。
在發動『索敵』的時候，雖然還不是很詳細，但好像技能會對此擅自加以判斷，作為自已同伴一方的存在，會以青色的點來表示，而與俺為敵的存在則會以紅色的點來表示。
托它的福，一下子就能夠判別出是敵是友。
但是，唔......就不能再獲取一些稍微更加詳細的情報嗎？
例如詳細地說明一下知道的技能。
在想著這種事情的瞬間，腦中那顆紅點的詳細情報被顯示出來了。

『奧爾嘉・卡爾梅利納』
出身：凱澤魯帝國
種族：獸人
性別：女
職業：暗殺者
年齡：８
Level：４５５
狀態：隸屬
稱號：黃昏的暗殺者（Twilight Assassin）
Three Sizes：──......

「Outtttttt！」

俺不禁吐槽起這顯示出來的情報。
根本連一點點的隱私意識都沒有啊，這個技能！
話說，不是說笑的，還真是出來了詳細到爆的資料！？
甚至連三圍都有！？
當然，三圍之後的部分，俺沒看喔。
俺的倫理觀把這種事給阻止了。
幹得好，俺！
攻擊力之類的還不清楚，但那應該也只能是用不一樣技能來調查吧？
嘛，即便如此，能弄出這些情報出來也已經是做得好到有點過了。
那個暫且不論，非常值得吐槽的地方是顯示出來的那些滿滿的資訊。
首先，出身是凱澤魯帝國的這件事。
那裡，是召喚了翔太他們的國家吧？
說到凱澤魯帝國，應該是因為最熱心於魔王討伐而進行勇者召喚的吧，這樣考慮的話，抱著想與魔族共存的想法的這個國家的國王，應該就成了他們的阻礙......之類的吧？
而且，暗殺者好像是女性的樣子呢。
加之，又是獸人，年齡也非常的年幼。
等級卻出乎意料的超過了４００級，這個也很奇怪。
但是，比起這些情報，俺更關注這個讓人在意的項目。

「這個隸屬狀態......是什麼啊？」

沒錯，就只有這一部分，俺完全搞不懂。
是那個嗎？
應該是奴隸的存在吧？
來到這個世界，因為還沒見過奴隸般的存在，原本還以為一定是沒有的呢......
如果，這個真的是奴隸之類的話，不禁擔心起翔太他們的安危。
因為有可能會像『勇者阿貝魯的日記』的內容一樣，被盡情利用完之後就被抹殺掉。
只是，俺對奴隸的概念也還是完全沒有頭緒。
即使是在地球那邊，雖然是存在像奴隸一樣的被驅使工作的情況，但真正的意味上的奴隸不可能會有的。
由於被勇者召喚的勇者中的某一位所流傳的技術，與地球近似的技術得也以普及開來，雖然俺是這麼想的，但果然，要是奴隸之類的真的存在的話，只能說是太落後了。
而且，不管怎樣，奴隸這樣的字眼，對於地球出身的俺而言，怎麼也開心不起來。
在想東想西的這段期間，暗殺者正朝著俺所在的訓練場過來。
所以，俺也自然而然地就把視線移向通向王城內部的入口處，突然，有個黑影以極猛的速度從那裡飛出來了。
看到那個影子的身姿，俺不禁脫口而出。

「嗯......還真是一副暗殺者的樣子啊。」
「嘶！？」

飛出來的黑影是，身著與俺相似的漆黑的長袍，小小隻的身型的人。
臉長怎樣是不知道啦。
這個黑影，毫無疑問就是暗殺者。
這邊是冷靜地看著暗殺者，但對方看到俺這個人則是大吃一驚。

「......為，為什麼會......我......！？」
「哈？」

應該是說了什麼話吧。
從這種氣氛看來，也只會說『為什會看得到我！？』這樣的話了。
對散發出那種氣氛的對方感到納悶的時候，長袍身姿的女性，從懷中取出了什麼東西，然後朝著俺投射過來了。

「！」
「誒！？等等啊！？突然間就攻擊了！？」

又變成討厭的感覺了。
嘛，確實啊，既然對方是狙擊這個國家的國王大人的暗殺者，那麼理所當然是有可能要把看到她的人給抹殺掉的。
這個先暫擱一邊，對方好像是對著俺投了兩把小刀過來。
這裡的話，應該普通地避開，好好地手下留情後將她無力化，也只能這樣做了吧......
俺看著朝著俺飛過來的小刀，一邊如此思考著──

「！？」
「咦！？」

俺的身體，就跟與露易絲小姐戰鬥的時候一樣，突然間動起來了。
接著，俺的身體用右掌的食指和中指以及中指和無名指分別把朝俺飛過來的兩把飛刀給夾住了，接著就這樣間不容髮地向對方投回去了。

「等！？」

雖然俺和魔物戰鬥並將其打倒過，但對現在的俺來說，並沒有與人類以及獸人互搏性命的覺悟。
即使一樣都是生命，但俺怎樣也不能把魔物和人認為一樣的。
雖然俺也知道這種想法非常的天真，但無論如何俺都不能那樣做。
正因為這樣，對於剛才俺的行動，俺自己感到萬分的恐懼。
露易絲小姐的那個時候，真的是因為不明道怎麼一回事，所以還不至於出現恐懼之類的那種感情，因此也算不上有什麼問題。
但是，現在不一樣。
恐怕，也許是技能『反射防衛』發動了，不過這樣會把暗殺者給殺掉的......
俺是這麼認為的。
可是，『反射防衛』技能比俺想像中的還要優秀。

「庫......！」

俺回投飛刀的速度，是無論如何都避開不了的速度。
所以，雖然原本是以為確實會貫穿暗殺者的身體......
但俺放出的飛刀只是貫穿了暗殺者的長袍，然後插進了城牆裡。
由於飛刀的速度和勁頭，暗殺者的長袍被硬拉著一起飛過去，而暗殺者的身體也跟著被縫在了城牆上面了。
一般而言，回投的飛刀把長袍貫穿就了事了，雖然俺是這麼想的，但進化之後的俺的身體，卻輕而易舉地做到了把對手的動作給封鎖住這樣的神技。
可以開心嗎？這個。
但是，正如技能名的意思，只是防衛就結束了，這次太好了。嗯。
硬是逼著自己認可的時候，被縫在牆上的暗殺者，一口氣把長袍給脫掉捨棄了，然後從被縫著的狀態中脫離出來。

「哦哦！」

然後，看到脫掉長袍的暗殺者真正的身姿，俺不禁發出了聲音。

「......。」

因為啊，暗殺者跟俺一樣都是黑色頭髮的。
而且，在頭上還有長著一對像貓耳一樣的獸耳，從腰部左右的位置伸出了一條顏色跟頭髮一樣是黑色的尾巴。
暗殺者毫不疏忽地盯著那樣的俺，因此俺也再一次好好確認一下這位暗殺者的樣貌。
黑色的短髮下面是一對跟貓一樣的金色眼睛。
因為是與年齡相符的幼女，所以非常的可愛。
但是，因為她半瞇著眼地瞪著俺，難得的可愛容顏都大打折扣了。
俺想她大概是黑貓的獸人吧。
因為不僅貓耳是黑色的，就連尾巴也是黑色的呢。
然後，至於她的服裝，包裹著身體的是如同特工裝束一樣的漆黑的衣服，頭上戴著一樣是黑色像是項鍊一樣的首飾。
雖然對這個世界是不是存在著忍者的概念不是很清楚，但存在暗殺者的裝束是可以肯定的。
哎呀......
就算是這樣，和俺一樣是黑色頭髮這一點，超安心的。
雖然被攻擊了。
多虧了她，所以也明白到沒必要對黑髮在意到那種地步。
對於這偶然確認的事實，俺正感到滿足之時，暗殺者以嚇人一跳的速度向俺逼近。
而且，她的手裡還握著小刀。

「──！」
「還要攻擊！？稍微冷靜一下好嗎！？」

這麼可愛的小孩子，拿著小刀殺過來是有多麼的可怕啊。
在內心盛大的咂著嘴，同時俺也為了避開而往註腳入力量，但就在這時。
俺的身體又再一次任性地動起來了。

「喂！等等！」

這完全就是被這技能給擺佈啊！
俺連控制自己的身體都做不到，用左手的手指夾住逼近過來的小刀，緊接著就保持著這樣用右手輕輕地碰了一下她的頭。

「──！！？？」

僅僅是這樣，蘿莉暗殺者就如同要昏厥一樣搖搖晃晃，接著就當場倒下了。

「哦哆！」

俺在蘿莉暗殺者倒在地面上之前就把她給抱住了。
哈哈哈......真的只是輕輕地碰了一下，但對方就暈倒了，是這樣的吧。
在臉上劃過的液體決對不會是淚水。
在想著這種白癡的事情的同時，俺再一次確認在俺胳膊中暈迷中的暗殺者的臉。
......唔。
這樣的睡著的身姿，與普通的小孩子沒有絲毫的不同啊。
這樣的小孩子，若無其事地拿著小刀，做著殺人的事，對於這樣的世界，俺的身體無意識地戰慄了。
至今為止，俺都沒想到會意識到這種事，但現在重新感受到異世界的可怕。
雖說並不是把魔物殺死，但即使是那樣，奪取性命的事變得理所當然，對變成這樣的自己感到十分的恐怖。
就算是比起得到飛躍性的身體數值，這個也是最為可怕的。
因為對手襲擊過來，所以把她擊倒了，最後如此考慮著的自己的身體，早已經處於異常狀態，這種事俺現在才察覺到。
但是，在這個世界，這是理所當然的，因為如果不這樣做就無法生存下去。
這裡已經......不是地球。
但就算大腦理解了，在心裡也仍舊是難以理解。
放鬆僵硬的肩膀，然後歎息著。
通過做這種事，鬆緩因緊張而發硬的身體，而此時再次從王城內部那邊，感覺到慌慌張張的氣息往這邊靠近。
往那個方向看過去，包括一臉嚴峻表情的露易絲小姐她們在內，連同『聖劍的女武神』的各位一同來到了這個訓練場。
露易絲小姐一確認到俺的身姿，就先行一步來到俺這邊。

「師傅！因為發生了突然的事態，實在非常抱歉，但這次能請您先回去嗎？」

......師傅的稱呼已經是決定事項了啊。

「好的，其實也沒什麼問題就是了......啊，露易絲小姐。」
「請稱呼我露易絲，也沒有必要使用敬語。」

輸給了露易絲小姐不容分說的迫力，俺聽從露易絲小姐的話。

「啊......唔，那麼就，露易絲，大概，俺是捉到了襲擊了國王大人的犯人......。」
「......哈？」

聽到俺的話，露易絲花了相當長的時間之後才這麼回問道。
嘛，這也是合情合理的啊。
突然就說俺把襲擊國王大人的暗殺者捉住了，誰都會嚇到的啊......
這麼想著的同時，俺也把自己抱著的蘿莉暗殺者讓她看一看。

「你看啊，這個孩子。因為剛剛她以強勁的勢頭從王城裡飛出來，接著還襲擊了俺......。」

......啊咧？
為什麼呢。
聽起來感覺怎麼盡是在找藉口啊。
但是，把那樣的俺擺在一邊的露易絲，一看到俺懷中抱著的蘿莉暗殺者就驚訝得睜大雙眼。
然後，短時間內，她對這個蘿莉投以如同觀察一樣的視線在看著。
在那個瞬間，俺看到了在這一瞬間露易絲的眼睛如同在發光一樣。
其中，俺對露易絲投向暗殺者蘿莉的視線有印象。
那是，在還沒有轉移到這個世界之前，在教室裡被大木隨意使用『鑒定』技能來調查俺數值的時候是一樣的。
在這一瞬之間，露易絲的眼睛看起來像是發光一樣，因為俺的技能『千里眼』發動了，所以露易絲使用了技能的事，以俺眼睛能看到的形式顯現出來，應該是這樣的吧。
大概是在調查蘿莉暗殺者的數值之類的吧。
話說，之前不是說露易絲不能使用技能嗎？
還是說，限定攻擊系的技能什麼的呢？
不管是哪邊，沒有使用技能就能斬出風之傷，在這個時間點上就已經非同一般了啊！

「這個這孩子是......羅娜！」
「是！有什麼吩咐？」

露出嚴肅表情的露易絲，呼喚在她身邊呆著的羅娜小姐。

「她的訊問就拜託你了！」
「瞭解！」

露易絲這麼吩咐後，就把這個蘿莉暗殺者交給了羅娜小姐。
接過蘿莉的羅娜小姐就這樣不知往什麼地方走掉了。
即使是那樣......訊問？
交給羅娜小姐來做沒關係嗎？
想著這種非常失禮的事情而歪頭不解之時，庫勞迪婭小姐往俺這邊靠近過來了。

「交給羅娜的話，是可以放心的喔，而且也不會對那個女孩子做什麼奇怪的事。」
「不，那個也沒什麼擔心的啦......不過交給羅娜小姐來詢問真的沒關係嗎？啊，不是啦，也不是說信不過她......。」

看到說著說著就不禁辯解起來的俺，庫勞迪婭小姐露出了苦笑。

「哈哈哈，你想說的話我明白啦。但是，她可是這個國家首席訊問官呢。」
「真的假的！？」
「而且，她還完美掌握了由公會的愛麗絲小姐所開辦的『SM講座』高級篇的所有內容。」
「在這裡竟然出現了愛麗絲小姐！？」

那種不知道是哪裡需要用到的講座，居然還有會去上課的人喔！
而且還是精通到高級篇那種程度，這超沒意義啊！？
說不定，會有去學習伽魯斯的更有效率地鍛鍊肌肉的方法的傢夥吧？
......不妙，否定不了。
這種看上去一點都不需要的講座，意外地活躍，對於這種事實，俺不寒而慄。

「這個暫且不論，誠一君，雖然對你很不好意思，不過這次必須得請你先回去才行。」
「那個，露易絲也說過呢。」
「是的，非常抱歉，師傅！明明應該由這邊來好好招呼您的......。」
「不，也沒必要那樣啦......不過，國王大人還好嗎？」

一聽到俺的這種問題，兩個人都同時浮現出了嚴峻的表情。

「......因為魔法師團的士兵們施加了回復魔法，因此保住了性命，但是......。」

對說不下去的露易絲感到困惑不解之時，由庫勞迪婭小姐接過她的話繼續說道。

「......陛下沒有醒過來。」
「誒？」

沒醒？剛剛露易絲是說了使用回復魔法保住了一命......
應該是體力的問題吧？
雖然試著如此推測，但那是錯的。

「因為誠一君已經知道了陛下遇襲的事，所以是可以告訴你......不過無論如何這次的事，都請閉嘴不談好嗎？」
「明，明白！其實最初就打算要跟誰說......。」
「那麼就告訴你吧。剛才襲擊陛下的暗殺者......多半在行刺的時候使用的凶器是『咒具』啊。」
「zhouju？」

聽到這陌生的詞語，正困惑不解之時，露易絲告訴俺。

「師傅，咒具正如其名，是指當中憑依了詛咒和惡靈的，以此來加害他人的道具和武器。在憑依了惡靈之類的咒具當中，也有輕易就可以令物主至死的那種程度的......但是這次的，好像是使用了被附加了叫做『悠久之長眠』的詛咒的刀具。」
「『悠久之長眠』......。」

詛咒，是嗎......
這麼說起來，阿爾也是由於身上附著『背負災厄之人』的詛咒，因此個人數值的運氣值變成了負數啊。
而且，阿爾說過的，解除那個詛咒的手段是沒有的。
也就是說──

「......陛下已經不可能再醒過來了。」
「......。」

庫勞迪婭小姐用著如同感情完全崩潰一樣的聲音如此說道。

「......不幸中的萬幸的是，第一王子羅伯魯特殿下，第二王子吉歐尼斯殿下，以及第一王女拉蒂絲大人全部都在學園裡面，因此應該沒受到狙擊吧。」

雖然庫勞迪婭小姐那麼說道，但那她依然陰暗的表情，完全看不出有什麼好慶倖的。
露易絲也因為沒能保護到國王大人而浮現出十分沉重的表情。
......有沒有俺能做的事呢......
俺想，這並不是引人注目與否的問題。
俺純粹是覺得這個國家非常的棒，即使只有那麼短短的一段時間，但已經變得非常喜歡了。
雖然不知道現任的國王或是前任的國王是不是實行著很好的政治。
即便如此，這個充滿笑臉的國家，看起來十分的耀眼奪目。
明明應該是這樣的，但庫勞迪婭小姐以及露易絲都露著一副陰沉的臉。
到剛才為止，明明都是歡快的氣氛，卻在一瞬之間被摧毀了。
這樣實在是太不講理了，俺對此忍無可忍。
所以，如果有俺能幫上忙的，俺想把力量借給她們。
在地球，因為盡是被人幫助的俺，連幫助什麼人之類的事都做不到。
但是，俺現在應該擁有能夠做到那種事的力量。
不對，是絕對有。
有沒有什麼可以解除詛咒的魔法，俺在腦中回憶著掌握到的魔法的所有效果。
可是，不管怎麼查看，也沒有解除詛咒的魔法。
......果然還是不行嗎？
就在這麼想著的時候。
俺注意到了，還有唯一一個方法。
那個方法就是──

「......沒有的話，創造就好了。」
「誒？」

聽到俺小聲地嘀咕著跟哪裡的王妃大人一樣的話，庫勞迪婭小姐以及露易絲都歪頭不解。
就是這樣......
這種時候不正是發揮俺的這種戲虐人的status之時嗎？
被技能玩弄，甚至連魔法都不能好好理解的俺。
至今為止，都不肯直視強大的力量，而是在逃避著現實。
最大限度地獲取技能，並持續著悲歎。
所以，俺想這次要好好地面對這些。
俺的技能......『魔法創造』。
這是俺直面俺的力量的第一步，也是第一次靠自己來創造出魔法。
俺用寄縮了強大意志的眼神，看向她們兩人──

「請把俺帶到國王大人那裡。」

──這樣告訴了她們。
